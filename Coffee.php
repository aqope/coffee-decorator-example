<?php
/** @author Artur Paklin */
/** @email paklin.artur@gmail.com */

interface IDrink {
    public function getPrice(): float;
}

class Drink implements IDrink {
    public function getPrice(): float {
        return 0.0;
    }
}

class Decorator implements IDrink {
    protected $beverage;
    
    public function __construct(IDrink $drink) {
        $this->beverage = $drink;
    }

    public function getPrice(): float {
        return $this->beverage->getPrice();
    }
}

class BlackCoffeeDecorator extends Decorator {
    public function getPrice(): float {
        $price = parent::getPrice();
        return 2.0 + $price;
    }
}

class MilkDecorator extends Decorator {
    public function getPrice(): float {
        $price = parent::getPrice();
        return 0.5 + $price;
    }
}

class CoffeeMachine {
    public function getCoffeePrice(IDrink $beverage): string {
        return $beverage->getPrice() . ' €';
    }
}

$drink = new Drink;
$blackCoffee = new BlackCoffeeDecorator($drink);
$coffeeWithMilk = new MilkDecorator($blackCoffee);

$cf = new CoffeeMachine();
echo 'Black Coffee Price: ' . $cf->getCoffeePrice($blackCoffee) . "\n";
echo 'Coffee with Milk Price: ' . $cf->getCoffeePrice($coffeeWithMilk);